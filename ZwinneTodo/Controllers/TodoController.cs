﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ZwinneTodo.Models;

namespace ZwinneTodo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TodoController : ControllerBase
    {
        private TodoContext _db;

        public TodoController(TodoContext db)
        {
            _db = db;
        }

        [HttpGet]
        public ActionResult<List<TodoItem>> Get()
        {
            return Ok(_db.Items.ToList());
        }

        [HttpGet("{id}")]
        public ActionResult<TodoItem> GetById(int id)
        {
            var item = _db.Items.Find(id);
            if(item == null)
            {
                return NotFound();
            }

            return Ok(item);
        }

        [HttpPost]
        public ActionResult Add([FromBody] TodoItem task)
        {
            task.CreatedAt = DateTime.Now;
            _db.Items.Add(task);
            _db.SaveChanges();
            return Ok();
        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, [FromBody] TodoItem task)
        {
            var item = _db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            item.Title = task.Title;
            item.Description = task.Description;

            _db.SaveChanges();

            return Ok();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var item = _db.Items.Find(id);
            if (item == null)
            {
                return NotFound();
            }

            _db.Items.Remove(item);
            _db.SaveChanges();

            return Ok();
        }
    }
}