import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodosComponent } from './todos/todos.component';
import { TodosViewComponent } from './todos/view/view.component';
import { TodosEditComponent } from './todos/edit/edit.component';

const routes: Routes = [
  {component: TodosComponent, path: '', pathMatch: 'full'},
  {component: TodosViewComponent, path: 'view/:id'},
  {component: TodosEditComponent, path: 'edit/:id'},
  {component: TodosEditComponent, path: 'new'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
