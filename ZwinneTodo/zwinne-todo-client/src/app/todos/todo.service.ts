import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { TodoItem } from './todo.models';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  private apiBase = "https://localhost:5001/api";

  constructor(private httpClient: HttpClient) { }

  getAll() {
    return this.httpClient.get<TodoItem[]>(`${this.apiBase}/todo`);
  }

  getOne(id: number) {
    return this.httpClient.get<TodoItem>(`${this.apiBase}/todo/${id}`);
  }

  add(item: TodoItem) {
    return this.httpClient.post(`${this.apiBase}/todo`, item);
  }

  update(id:number, item: TodoItem) {
    return this.httpClient.put(`${this.apiBase}/todo/${id}`, item);
  }

  delete(id: number) {
    return this.httpClient.delete(`${this.apiBase}/todo/${id}`);
  }
}
