import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';
import { TodoItem } from '../todo.models';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-todos-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class TodosEditComponent implements OnInit {

  item: TodoItem;

  constructor(private activatedRoute: ActivatedRoute, private todoService: TodoService, private router: Router) { }

  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('id');
      if (id && !Number.isNaN(+id)) {
        this.todoService.getOne(+id).subscribe(item => {
          this.item = item;
        });
      } else {
        this.item = { title: "", description: "" };
      }
    });
  }

  save() {
    let result: Observable<any>;
    if(this.item.id) {
      result = this.todoService.update(this.item.id, this.item);
    } else {
      result = this.todoService.add(this.item);
    }

    result.subscribe(() => {
      this.router.navigate(['']);
    });
  }

}
